mem_test: main.o PRNG.o ObjectAllocator.o
	g++ -o mem_test main.o PRNG.o ObjectAllocator.o

main.o: main.cpp ObjectAllocator.h
	g++ -c main.cpp

PRNG.o: PRNG.cpp PRNG.h
	g++ -c PRNG.cpp
	
ObjectAllocator.o: ObjectAllocator.cpp ObjectAllocator.h
	g++ -c ObjectAllocator.cpp