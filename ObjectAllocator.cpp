/******************************************************************************/
/*!
\file	ObjectAllocator.cpp
\author Robin Barranda
\par	email: robin.5045(@)yahoo.com
\date	5/28/2017
\brief
	This file contains the implementation of the following functions for the
	ObjectAllocator class:

	Public Operations of the ObjectAllocator class include:
		ObjectAllocator()
		~ObjectAllocator()

		void *Allocate(const char *)
		void Free(void *)

		unsigned DumpMemoryInUse(DUMPCALLBACK) const
		unsigned ValidatePages(VALIDATECALLBACK) const
		unsigned FreeEmptyPages(void)

		SetDebugState(bool)
		const void *GetFreeList() const
		const void *GetPageList() const
		OAConfig GetConfig() const
		OAStats GetStats() const

Specific portions that gave you the most trouble:
-Double-checking comments
-Organizing Doxygen tags



*/
#include "ObjectAllocator.h"
#include <iostream> /* std::cout */
#include <cstring>  /* std::strcpy, std::strlen, std::memset */

/**************************************************************************/
/*!

\brief
	This is the constructor for the ObjectAllocator class.

\param
	ObjectSize - size of the struct containing client-defined data

\param
	config - configuration settings for this ObjectAllocator instance

*/
/**************************************************************************/
ObjectAllocator::ObjectAllocator(size_t ObjectSize, const OAConfig & config)
	: PageList_(0), FreeList_(0), segmentSize_(0), startSize_(0),
	debugMode_(false)
{
	// copy config to internal config_
	config_ = config;

	// determine objectsize per block
	stats_.ObjectSize_ = ObjectSize;

	// initialize stats_ variables
	InitStats();

	// initialize alignment-related variables
	InitAlignment();

	// initialize error messages
	InitErrorMsgs();

	// create a block of memory and divide -- representing a page
	CreatePage();
}
/**************************************************************************/
/*!

\brief
	This is the destructor of the ObjectAllocator class.

*/
/**************************************************************************/
ObjectAllocator::~ObjectAllocator()
{
	GenericObject *pageJumper = PageList_;

	// check every page
	while (pageJumper != NULL) {

		// if header is external, need to clear dynamically allocated memory
		if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
		{
			// check every block, use char for 1 byte pointer arithmetic 
			char *address = reinterpret_cast<char *>(pageJumper) + startSize_;

			// begin traversing page until objects per page or objects in use
			for (unsigned i = 0; i < config_.ObjectsPerPage_ ||
				stats_.ObjectsInUse_ != 0; ++i) {

					// if memory is in use, free the block
					if (GetHeaderFlag(address + (i * segmentSize_)) == true)
						Free(address + (i * segmentSize_));
			}
		}
		// delete the empty page
		char *temp = reinterpret_cast<char *>(pageJumper);
		pageJumper = pageJumper->Next;
		delete[] temp;
	}
}

/**************************************************************************/
/*!

\brief
	This is a public function that the client will call to request for a block
	of memory.

\param
	label - client-defined string used for external headers

\return
	Pointer to the block of memory divided by the memory manager.

*/
/**************************************************************************/
void * ObjectAllocator::Allocate(const char *label)
{
	// use CPP memory manager 
	if (config_.UseCPPMemManager_ == true) {
		// update statistics
		++stats_.Allocations_;
		if (stats_.Allocations_ > stats_.MostObjects_)
			++stats_.MostObjects_;

		// return allocated memory with new
		try {
			return new char[stats_.ObjectSize_];
		}
		catch (...) {
			throw OAException(OAException::E_NO_MEMORY, no_mem);
		}
	}

	// if no more free objects, create new page
	if (stats_.FreeObjects_ <= 0)
		CreatePage();

	// retrieve head
	GenericObject *obj = FreeList_;

	// release from linked list
	FreeList_ = FreeList_->Next;

	// set allocated signature only if debug is on
	if (config_.DebugOn_ == true)
		std::memset(reinterpret_cast<char *>(obj), 
			ALLOCATED_PATTERN,	stats_.ObjectSize_);

	// update stats
	--stats_.FreeObjects_;
	++stats_.ObjectsInUse_;
	++stats_.Allocations_;
	if (stats_.Allocations_ > stats_.MostObjects_)
		++stats_.MostObjects_;

	// set header of the block before passing to client
	SetHeader(reinterpret_cast<char *>(obj), label);

	// pass the block of memory to the client
	return obj;
}

/**************************************************************************/
/*!

\brief
	This is a public function that the client will call to free a memory 
	previously requested.

\param
	Object - address of memory to free or give back to the memory manager

*/
/**************************************************************************/
void ObjectAllocator::Free(void *Object)
{
	// use CPP memory manager 
	if (config_.UseCPPMemManager_) {

		// update stats
		++stats_.Deallocations_;

		// delete object allocated by new
		delete[] reinterpret_cast<char*>(Object);

		// exit the function
		return;
	}

	// if DebugOn_ activated
	if (config_.DebugOn_ == true) {

		// check for bad boundary by check if a pointer was passed
		if (!ExistsOnPage(Object))
			throw OAException(OAException::E_BAD_BOUNDARY, bad_boundary);

		// check for multiple frees
		if (ExistsOnFree(Object))
			throw OAException(OAException::E_MULTIPLE_FREE, multiple_frees);
	}

	// set padding signature only if debug is on
	if (config_.DebugOn_ == true) {
		std::memset(reinterpret_cast<char *>(Object), FREED_PATTERN, stats_.ObjectSize_);
	}

	// add node to FreeList_
	GenericObject *obj = reinterpret_cast<GenericObject *>(Object);
	obj->Next = FreeList_;
	FreeList_ = obj;

	// update stats
	++stats_.FreeObjects_;
	--stats_.ObjectsInUse_;
	++stats_.Deallocations_;

	// update (clear) the header's bit flag
	ClearHeader(reinterpret_cast<char*>(obj));

	// check if client touched memory that was not allocated to them
	ValidatePages(0);
}

/**************************************************************************/
/*!

\brief
	This is a public callback function that checks for blocks on all the pages
	if they are in use and uses a client-defined function to processes the 
	information.

\param
	fn - a function pointer that takes a const void * and size_t as parameters
    to process information about blocks of memory that are still in-use by 
	the client.

\return
	The number of blocks that the client still holds

*/
/**************************************************************************/
unsigned ObjectAllocator::DumpMemoryInUse(DUMPCALLBACK fn) const
{
	// prepare pointer for browsing
	GenericObject * pageJumper = PageList_;
	int inUse = 0;	// variable to count number of blocks used

					// loop through the whole data structure
	while (pageJumper != NULL) {

		// cast to char for 1 byte pointer arithmetic
		char * memory = reinterpret_cast<char *>(pageJumper) + startSize_;

		// loop through the current page
		for (unsigned i = 0; i < config_.ObjectsPerPage_; ++i) {

			// check if header's indicator is 1
			if (GetHeaderFlag(memory + i * segmentSize_)) {
				(*fn)(memory + i * segmentSize_, stats_.ObjectSize_);
				++inUse;
			}
		}

		// move to the next page
		pageJumper = pageJumper->Next;
	}

	// return number of blocks still in-use by the client
	return inUse;
}

/**************************************************************************/
/*!

\brief
	This is a public callback function that checks if any blocks used as 
	padding have been tampered with by the client. It then uses the 
	client-defined function to process blocks that have been found to be 
	corrupted.

\param
	fn - a function pointer that takes a const void * and a size_t as 
	parameters to process information about allocate-able blocks that have 
	been corrupted

\return
	The number of corrupted blocks

*/
/**************************************************************************/
unsigned ObjectAllocator::ValidatePages(VALIDATECALLBACK fn) const
{
	// if debug off or padbytes 0, no need to check what doesnt exist
	if (config_.DebugOn_ == false || config_.PadBytes_ == 0)
		return 0;

	// traverse all page list and check left and right of each block
	GenericObject *pageJumper = PageList_;

	// variable to accumulate number of potentially bad blocks
	int counter = 0;

	while (pageJumper != NULL) {

		// cast to char for 1 byte per addition, move to the first block
		char * memory = (reinterpret_cast<char *>(pageJumper) + startSize_);

		// boolean variables to keep track of the status of left and right pads
		bool checkLeft;
		bool checkRight;

		// trarverse the page
		for (unsigned i = 0; i < config_.ObjectsPerPage_; ++i)
		{
			// reset boolean variables
			checkLeft = true;
			checkRight = true;

			// check left pads, move pointer to the next block
			if (!(checkLeft = ValidateLeft(memory + (i * segmentSize_)))) {

				// if a leak was found on the left
				++counter;

				// if a callback function was provided
				if (fn != NULL)	
					(*fn)(memory + (i * segmentSize_), stats_.ObjectSize_);
			}
			// check right pads
			else if (!(checkRight = ValidateRight(memory + (i * segmentSize_) +
				(stats_.ObjectSize_ - 1)))) {

				// if a leak was found on the right
				++counter;

				// if a callback function was provided
				if (fn != NULL)
					(*fn)(memory + (stats_.ObjectSize_ - 1) +
					(i * segmentSize_), stats_.ObjectSize_);
			}
			// if a callback function was not provided, this function was called by Free()
			if (fn == NULL)
				if (!checkLeft || !checkRight)
					throw OAException(OAException::E_CORRUPTED_BLOCK, corrupted_block);
		}

		// move to next page
		pageJumper = pageJumper->Next;
	}

	// return number of corrupted blocks
	return counter;
}

/**************************************************************************/
/*!

\brief
	This is a public function that frees pages that are empty to save memory.

\return
	The number of pages deleted

*/
/**************************************************************************/
unsigned ObjectAllocator::FreeEmptyPages()
{
	GenericObject *pageJumper = PageList_;
	GenericObject *prevPage = pageJumper;
	GenericObject *temp = NULL;
	int numDeleted = 0;

	// traverse the linked list
	while (pageJumper != NULL)
	{
		// check if page is empty
		if (CheckIfEmptyPage(reinterpret_cast<char*>(pageJumper))) {

			// is prevPage is on the head (PageList_), move the head
			if (pageJumper == PageList_)
			{
				// prepare address to delete
				temp = pageJumper;

				// move pageJumper to the destination node
				pageJumper = pageJumper->Next;

				// set the new head
				PageList_ = pageJumper;

				// set prevPage pointer
				prevPage = pageJumper;
			}
			// else move pageJumper and connect with prevPage
			else
			{
				// prepare address to delete
				temp = pageJumper;

				// move pageJumper to the next destination
				pageJumper = pageJumper->Next;

				// link with prev node
				prevPage->Next = pageJumper;

			}

			// delete the page
			FreePage(reinterpret_cast<char*>(temp));

			++numDeleted;
		}
		// else check next page
		else
		{
			// prevPage tracks previous page
			prevPage = pageJumper;

			// move on to next page to check
			pageJumper = pageJumper->Next;
		}
	}

	// return the number of pages that were deleted
	return numDeleted;
}


/**************************************************************************/
/*!

\brief
	This is a public function that toggles debug mode. Use this to be able to
	view addresses that are retrieved from the operating system.

\param
	State - true to enable debugging, false to disable

*/
/**************************************************************************/
void ObjectAllocator::SetDebugState(bool State)
{
	debugMode_ = State;

	// if the feature has been turned on
	if (State)
	{
		// prints addresses of pages within the list
		std::cout << "Addresses of Pages Allocated:\n";
		PrintPageList();

		// prints addresses of allocate-able blocks within the list
		std::cout << "Addresses of items on the free list.\n";
		PrintFreeList();
	}
}

/**************************************************************************/
/*!

\brief
	This is a public getter function that retrieves the head of the list of
	blocks that are allocate-able by the manager.

\return
	A const pointer to the head of the linked list

*/
/**************************************************************************/
const void * ObjectAllocator::GetFreeList() const
{
	return FreeList_;
}

/**************************************************************************/
/*!

\brief
	This is a public getter function that retrieves the head of the list of
	individual pages that were requested from the operating system.

\return
	A const pointer to the head of the linked list

*/
/**************************************************************************/
const void * ObjectAllocator::GetPageList() const
{
	return PageList_;
}

/**************************************************************************/
/*!

\brief
	This is a public getter function to retrieve the configuration of this
	class instance.

\return
	An OAConfig object containing the configuration settings

*/
/**************************************************************************/
OAConfig ObjectAllocator::GetConfig() const
{
	return config_;
}

/**************************************************************************/
/*!

\brief
	This is a public getter function that retrieves the current statistics
	of this class instance.

\return
	An OAStats object containing the statistics of this class instance


*/
/**************************************************************************/
OAStats ObjectAllocator::GetStats() const
{
	return stats_;
}

/**************************************************************************/
/*!

\brief
	This is a function to initialize and calculate for Alignment related 
	variables.

*/
/**************************************************************************/
void ObjectAllocator::InitAlignment()
{
	if (config_.Alignment_ > 1)
	{
		// set alignment padding of beginning section
		InitStartAlignment();

		// set alignment padding of each segment
		InitSegmentAlignment();
	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function to calculate the LeftAlignSize_ variable and to
	update neccessary statistics

*/
/**************************************************************************/
void ObjectAllocator::InitStartAlignment()
{
	// calculate how much padding will be needed
	while (startSize_ % config_.Alignment_ != 0)
	{
		++startSize_;
		++config_.LeftAlignSize_;
		++stats_.PageSize_;
	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function to calculate the InterAlignSize_ variable and to
	update necessary statistics

*/
/**************************************************************************/
void ObjectAllocator::InitSegmentAlignment()
{
	// calculate how much padding will be needed
	while (segmentSize_ % config_.Alignment_ != 0)
	{
		++segmentSize_;
		++config_.InterAlignSize_;
	}

	// add to page size for (ObjectsPerpage_ - 1) times
	stats_.PageSize_ += (config_.InterAlignSize_ * (config_.ObjectsPerPage_ - 1));
}

/**************************************************************************/
/*!

\brief
	This is a function that sets the error messages to be when throwing
	exceptions.

*/
/**************************************************************************/
void ObjectAllocator::InitErrorMsgs()
{
	// for OAException::E_NO_MEM
	no_mem = "Critial Error: System has run out of RAM.";

	// for OAException::E_NO_PAGES
	no_pages = "Logical memory has run out: Cannot allocate unless existing "
		"object is freed first.";

	// for OAException::E_BAD_BOUNDARY
	bad_boundary = "Error: Attempting to free an address that was not "
		"allocated by the manager.";

	// for OAException::E_MULTIPLE_FREE
	multiple_frees = "Error: Attempting to free an allocation that has "
		"already been freed.";

	// for OAException::E_CORRUPTED_BLOCK
	corrupted_block = "Corruption detected: Memory not allocated by the "
		"manager has been modified.";
}

/**************************************************************************/
/*!

\brief
	This is a function that initializes the necessary statistics stored within
	this class instance.

*/
/**************************************************************************/
void ObjectAllocator::InitStats()
{
	// Allocate memory for the pointer
	stats_.PageSize_ += sizeof(void*);
	startSize_ += sizeof(void*);

	// Allocate memory for objects
	stats_.PageSize_ += (stats_.ObjectSize_ * config_.ObjectsPerPage_);
	segmentSize_ += stats_.ObjectSize_;

	// Allocate memory for the padding
	stats_.PageSize_ += (2 * config_.PadBytes_ * config_.ObjectsPerPage_);
	segmentSize_ += 2 * config_.PadBytes_;
	startSize_ += config_.PadBytes_;

	// Allocate memory for the header
	stats_.PageSize_ += (config_.HBlockInfo_.size_ * config_.ObjectsPerPage_);
	segmentSize_ += config_.HBlockInfo_.size_;
	startSize_ += config_.HBlockInfo_.size_;
}

/**************************************************************************/
/*!

\brief
	This is a helper function that initializes memory of header blocks when the
	memory manager divides the page.

\param
	memory - address of the allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::InitHeader(char * memory)
{
	// clears the basic header block section if applicable
	if (config_.HBlockInfo_.type_ == OAConfig::hbBasic ||
		config_.HBlockInfo_.type_ == OAConfig::hbExtended)
		ClearHeaderBasic(memory);

	// clears the extended header block section
	if (config_.HBlockInfo_.type_ == OAConfig::hbExtended) {
		ClearHeaderCounter(memory);
		ClearHeaderExtended(memory);
	}

	// clears the external header's pointer and all allocated memory
	if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
		ClearHeaderExternal(memory);
}

/**************************************************************************/
/*!

\brief
	This is a function that creates a new page to that the memory manager will
	divide into allocate-able blocks.

*/
/**************************************************************************/
void ObjectAllocator::CreatePage()
{
	// throw an exception if logical max memory has been reached
	if (stats_.PagesInUse_ >= config_.MaxPages_)
		throw OAException(OAException::E_NO_PAGES, no_pages);

	char *page = 0;

	// Allocate Memory using PageSize_
	try {
		page = new char[stats_.PageSize_];
	}
	catch (...) {
		throw OAException(OAException::E_NO_MEMORY, no_mem);
	}

	// update stats to add PageInUse
	++stats_.PagesInUse_;

	if (config_.DebugOn_ == true)
		// set all bytes in newly created page to AA
		std::memset(page, UNALLOCATED_PATTERN, stats_.PageSize_);

	// casting a page to a GenericObject * and adjusting pointers
	GenericObject *obj = reinterpret_cast<GenericObject *>(page);

	// Connect new node to head
	obj->Next = PageList_;

	// set new node as head
	PageList_ = obj;

	// divide the page
	DividePage(page);

	// print the page if debug mode is on
	if (debugMode_)
		PrintPage(page);
}

/**************************************************************************/
/*!

\brief
	This is a function that divides the page into blocks

\param
	memory - the beginning address of the page

*/
/**************************************************************************/
void ObjectAllocator::DividePage(char *memory)
{
	// block accumulator to indicate pointer position per loop iteration
	size_t block = sizeof(void*);

	// set alignment signature only if in debug mode
	if (config_.DebugOn_)
		std::memset(reinterpret_cast<GenericObject*>(memory + block),
			ALIGN_PATTERN, config_.LeftAlignSize_);

	// update position - add left alignment
	block += config_.LeftAlignSize_;

	// loop to divide the block starting at page+sizeof(void*)
	for (unsigned i = 0; i < config_.ObjectsPerPage_; ++i)
	{
		// update position - add header size
		block += config_.HBlockInfo_.size_;

		// set padbytes only if in debug mode
		if (config_.DebugOn_)
			std::memset(reinterpret_cast<GenericObject*>(memory + block),
				PAD_PATTERN, config_.PadBytes_);

		// update position - add padding
		block += config_.PadBytes_;

		////////////// OBJECT ALLOCATION SECTION ///////////////////
		// moves head (FreeList_) to next block
		GenericObject *obj = reinterpret_cast<GenericObject *>(memory + block);
		obj->Next = FreeList_;
		FreeList_ = obj;

		// update stats to add free objects
		++stats_.FreeObjects_;

		// initialize header if applicable
		if (config_.HBlockInfo_.type_ != OAConfig::hbNone)
			InitHeader(reinterpret_cast<char*>(obj));

		// update position - add object size
		block += (stats_.ObjectSize_);
		////////////////////////////////////////////////////////////
		// set padbytes only if in debug mode
		if (config_.DebugOn_)
			std::memset(reinterpret_cast<GenericObject*>(memory + block),
				PAD_PATTERN, config_.PadBytes_);

		// update position - add padding
		block += config_.PadBytes_;

		// update position except for the last block
		if (i < config_.ObjectsPerPage_ - 1) {
			// set alignment signature ony if in debug mode
			if (config_.DebugOn_)
				std::memset(reinterpret_cast<GenericObject*>(memory + block),
					ALIGN_PATTERN, config_.InterAlignSize_);

			// update position - add inter-block alignment
			block += config_.InterAlignSize_;
		}

	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function that sets the header of an allocate-able memory
	block.

\param
	memory - beginning address of an allocate-able block

\param
	label - a string to use for the external header's pointed-to struct

*/
/**************************************************************************/
void ObjectAllocator::SetHeader(char * memory, const char * label)
{
	// set header stats only if block has been allocated/activated
	if (config_.HBlockInfo_.type_ == OAConfig::hbBasic ||
		config_.HBlockInfo_.type_ == OAConfig::hbExtended)

		// after allocation counter has been increased, update header counter
		IncHeaderBasic(memory);

	// increase the counter for extended headers
	if (config_.HBlockInfo_.type_ == OAConfig::hbExtended)
		IncHeaderCounter(memory);

	// to create MemBlockInfo ptr
	if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
		SetHeaderExternal(memory, label);
}

/**************************************************************************/
/*!

\brief
	This is a helper function that clears the header block whenever a
	client-requested memory is freed and returned to the memory manager.

\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::ClearHeader(char * memory)
{
	// clear header blocks
	if (config_.HBlockInfo_.type_ == OAConfig::hbBasic ||
		config_.HBlockInfo_.type_ == OAConfig::hbExtended)
		ClearHeaderBasic(memory);

	if (config_.HBlockInfo_.type_ == OAConfig::hbExtended)
		ClearHeaderExtended(memory);

	if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
		ClearHeaderExternal(memory);
}

/**************************************************************************/
/*!

\brief
	This is a helper function that retrieves the address of a block's external
	header

\param
	memory - address of an allocate-able block of memory

\return
	Beginning address of the block's external header

*/
/**************************************************************************/
char * ObjectAllocator::GetHeaderExternalPtr(char * memory) const
{
	// return the header pointer
	return GetHeaderPtr(memory);
}

/**************************************************************************/
/*!

\brief
	This is a function to clear the external header's pointer and all 
	dynamically allocated memory that it has previously obtained.

\param
	memory - beginning addres of allocate-able block of memory

*/
/**************************************************************************/
void ObjectAllocator::ClearHeaderExternal(char * memory)
{
	// retrieve address of header block
	char * address = GetHeaderPtr(memory);

	// cast the pointer to poitner address to a MemBlockInfo pointer
	MemBlockInfo *ptr = reinterpret_cast<MemBlockInfo*>(
		*reinterpret_cast<MemBlockInfo**>(address)
		);

	// use unsigned char to be able to compare to the pattern
	if (ptr != NULL && 
		*(reinterpret_cast<unsigned char*>(address)) != UNALLOCATED_PATTERN) {

		// delete the label if an new was used
		if (ptr->label != NULL)
			delete[] ptr->label;

		// delete the allocated MemBlockInfo
		delete ptr;
	}

	// clear the header
	std::memset(address, 0x00, config_.HBlockInfo_.size_);
}

/**************************************************************************/
/*!

\brief
	This is a function that sets the external header of an allocate-able block.

\param 
	memory - beginning address of an allocate-able block

\param
	label - the string to set as the external header's label

*/
/**************************************************************************/
void ObjectAllocator::SetHeaderExternal(char * memory, const char * label)
{
	MemBlockInfo* memInfo = 0;

	// allocate MemBlockInfo struct on a variable
	try
	{
		memInfo = new MemBlockInfo;
	}
	catch (...) {
		throw OAException(OAException::E_NO_MEMORY, no_mem);
	}

	// check if label is empty
	if (label != NULL)
	{
		// allocate memory for the label and copy label
		memInfo->label = new char[strlen(label) + 1];
		std::strcpy(memInfo->label, label);
	}
	else
	{
		memInfo->label = NULL;
	}

	// assign all variables
	memInfo->in_use = true;
	memInfo->alloc_num = stats_.Allocations_;

	// Use block of memory as pointer to a pointer
	MemBlockInfo** address = 
		reinterpret_cast<MemBlockInfo**>(GetHeaderExternalPtr(memory));

	// assign memInfo's address to the external header's pointer
	*address = memInfo;

}

/**************************************************************************/
/*!

\brief
	This is a function that clears the extended header of an allocate-able 
	block.

\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::ClearHeaderExtended(char * memory)
{
	// get beginning of header block
	char * address = GetHeaderPtr(memory);

	// clear additional bytes specified by user
	std::memset(address, 0x00, config_.HBlockInfo_.additional_);
}

/**************************************************************************/
/*!

\brief
	This is a function that clears the counter of an extended header (Used 
	only when initializing an extended header block)

\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::ClearHeaderCounter(char * memory)
{
	// get the address
	char *counter = GetHeaderCounterPtr(memory);

	// set to 0
	*(reinterpret_cast<unsigned short*>(counter)) = 0;
}

/**************************************************************************/
/*!

\brief
	This is a function that increases the counter of an extended header.
	
\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::IncHeaderCounter(char * memory)
{
	// get the address
	char *counter = reinterpret_cast<char*>(GetHeaderCounterPtr(memory));

	// increment by 1
	++*(reinterpret_cast<unsigned short*>(counter));
}

/**************************************************************************/
/*!

\brief
	This is a function that sets the allocation number of the basic header
	component of a header (Can also be used for extended headers).

\param 
	memory - beginning address of an allocate-able block
*/
/**************************************************************************/
void ObjectAllocator::IncHeaderBasic(char * memory)
{
	// retrieve pointer to header
	char *header = reinterpret_cast<char*>(GetHeaderAllocPtr(memory));

	// assign the allocation number to it
	*(reinterpret_cast<unsigned int*>(header)) = stats_.Allocations_;

	// Activate the byte's big flag
	*(header + sizeof(int)) |= 1;
}

/**************************************************************************/
/*!

\brief
	This is a function that clears the basic header component of a header
	(Can also be used for extended headers)

\param
	memory - beginning address of an allocate-able block of memory

*/
/**************************************************************************/
void ObjectAllocator::ClearHeaderBasic(char * memory)
{
	// use the block of memory allocated for the header as an integer
	char *header = GetHeaderAllocPtr(memory);

	// clear the bytes
	*(reinterpret_cast<unsigned int*>(header)) = 0;

	// set the flag back to 0
	*(header + sizeof(int)) &= 0;
}

/**************************************************************************/
/*!

\brief
	This is a function that retireves the header pointer of an allocate-able
	block of memory

\param
	memory - beginning address of an allocate-able block

\return
	The beginning address of the block's header

*/
/**************************************************************************/
char * ObjectAllocator::GetHeaderPtr(char * memory) const
{
	return (memory - (config_.PadBytes_ + config_.HBlockInfo_.size_));
}

/**************************************************************************/
/*!

\brief
	This is a function that retrieves the address of the counter of an 
	extended header.

\param
	memory - beginning address of an allocate-able block

\return
	The address of the counter section of an extended header

*/
/**************************************************************************/
char * ObjectAllocator::GetHeaderCounterPtr(char * memory) const
{
	// retrieve start of header
	char * address = GetHeaderPtr(memory);

	// move by config header block's additional size
	address += config_.HBlockInfo_.additional_;

	return address;
}

/**************************************************************************/
/*!

\brief
	This function retrieves the address of the alloction numbering section
	on the basic header component (Can also be used with extended headers).

\param
	memory - beginning address of allocate-able block

\return
	The beginning address of the basic header block

*/
/**************************************************************************/
char * ObjectAllocator::GetHeaderAllocPtr(char * memory) const
{
	// calculate front of header
	char * address = GetHeaderPtr(memory);

	// move pointer by difference of size and the allocation block
	address += (config_.HBlockInfo_.size_ - (OAConfig::BASIC_HEADER_SIZE));
	return address;
}

/**************************************************************************/
/*!

\brief
	This is a helper function to check if an address exists on the free list.

\param
	memory - beginnig address of an allocate-able block

\return
	True if block is free, false if block is in use by the client

*/
/**************************************************************************/
bool ObjectAllocator::ExistsOnFree(void *memory)
{
	GenericObject *obj = FreeList_;

	// loop through the FreeList_;
	while (obj != NULL && obj != memory)
		obj = obj->Next;

	// check result
	if (obj == NULL)
		return false;	// no match
	else
		return true;	// address match found
}

/**************************************************************************/
/*!

\brief
	This is a helper function that checks if an addresses is the beginning
	of an allocate-able block

\param
	memory - any address

\return
	True if the memory passed is valid, False if the memory passed is invalid

*/
/**************************************************************************/
bool ObjectAllocator::ExistsOnPage(void *memory)
{
	// iterate through PageList and check where address falls under
	for (GenericObject* it = PageList_; it != NULL; it = it->Next) {

		// cast to char for 1 byte pointer arithmetic
		if (it <= memory && memory
			<= (reinterpret_cast<char*>(it) + stats_.PageSize_ - 1))	
		{
			// use arithmetic and % to determine if address is valid
			size_t size = 
				(static_cast<char*>(memory) - reinterpret_cast<char*>(it));

			// subtract pointer by left offset
			size -= sizeof(void*);				// pointer
			size -= config_.PadBytes_;			// padbytes
			size -= config_.LeftAlignSize_;		// left alignment size
			size -= config_.HBlockInfo_.size_;	// header

			// use modulo to determine if pointer is on the right location
			// by the distance between desired memory locations
			if ((size % segmentSize_) == 0)

				// the address is valid
				return true;
			else

				// the address is invalid
				return false;
		}
	}

	// returns invalid if the address passed does not exist anywhere
	return false;
}

/**************************************************************************/
/*!

\brief
	This is a helper function to check whether a block is in use by checking
	its header.

\param
	memory - beginning address of an allocate-able block

\return
	True if the block is in use, False if it is free

*/
/**************************************************************************/
bool ObjectAllocator::GetHeaderFlag(char * memory) const
{
	// for basic and extended headers
	if (config_.HBlockInfo_.type_ == OAConfig::hbBasic ||
		config_.HBlockInfo_.type_ == OAConfig::hbExtended) {

		// get the address of the flag
		unsigned char * address = reinterpret_cast<unsigned char*>(
			GetHeaderAllocPtr(memory) + sizeof(int)
			);

		if (*address & 1)
			return true;	// in use
		else
			return false;	// not in use
	}

	// for external headers
	else if (config_.HBlockInfo_.type_ == OAConfig::hbExternal) {

		// retrieve a pointer to the previously allocated MemBlockInfo struct
		MemBlockInfo* address = reinterpret_cast<MemBlockInfo*>(
			*reinterpret_cast<MemBlockInfo**>(GetHeaderExternalPtr(memory))
			);

		// check for NULL condition just in case
		if (address == NULL)
			return false;	// not in use
		else if (address->in_use == true)
			return true;	// in use
		else
			return false;	// not in use
	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function the check the left padding of an allocate-able
	block of memory

\param
	memory - beginning address of an allocate-able block

\return
	True if the block has not corrupted anything, False if a padding corruption
	has been found

*/
/**************************************************************************/
bool ObjectAllocator::ValidateLeft(char * memory) const
{
	// loop through as many as size of PadBytes_
		for (unsigned i = 1; i <= config_.PadBytes_; ++i)
	{
		// subtract to address to move to the left
		if (*reinterpret_cast<unsigned char *>(memory - i) != PAD_PATTERN) {

			// corruption found
			return false;
		}
	}

	// no corruption found
	return true;
}

/**************************************************************************/
/*!

\brief
	This is a helper function the check the right padding of an allocate-able
	block of memory

\param
	memory - beginning address of an allocate-able block

\return
	True if the block has not corrupted anything, False if a padding corruption
	has been found


*/
/**************************************************************************/
bool ObjectAllocator::ValidateRight(char * memory) const
{
	// loop through as many as size of PadBytes_
	for (unsigned i = 1; i <= config_.PadBytes_; ++i)
	{
		// add to address to move right
		if (*reinterpret_cast<unsigned char *>(memory + i) != PAD_PATTERN) {

			// corruption found
			return false;
		}
	}

	// no corruption found
	return true;
}

/**************************************************************************/
/*!

\brief
	This is a helper function that frees a page and updates the necessary
	variables.

\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::FreePage(char * memory)
{
	// loop through free list
	// for every free list found within the page, delete
	GenericObject *freeJumper = FreeList_;
	GenericObject *prevFree = freeJumper;
	while (freeJumper != NULL)
	{
		// test if a free list element is within the page
		if (static_cast<void*>(memory) <= reinterpret_cast<void*>(freeJumper) 
			&& reinterpret_cast<char*>(freeJumper) < 
			static_cast<void*>(memory + stats_.PageSize_))
		{
			// if node to delete is the head
			if (freeJumper == FreeList_)
			{
				// move node to next
				freeJumper = freeJumper->Next;

				// set the new head
				FreeList_ = freeJumper;

				// set the prev pointer
				prevFree = freeJumper;
			}
			else
			{
				// move node to next
				freeJumper = freeJumper->Next;

				// connect the nodes
				prevFree->Next = freeJumper;
			}

			// remove from free list stats
			--stats_.FreeObjects_;
		}
		else
		{
			// move the pointers
			prevFree = freeJumper;

			freeJumper = freeJumper->Next;
		}
	}

	// delete the page
	delete[] memory;

	// update stats
	--stats_.PagesInUse_;
}

/**************************************************************************/
/*!

\brief
	This is a helper function to check if a page is empty.

\param
	memory - beginning address of an allocate-able block

\return
	True if the page is empty, False if at least one block is in use

*/
/**************************************************************************/
bool ObjectAllocator::CheckIfEmptyPage(char * memory)
{
	// point to the start of allocate-able block
	char * address = (memory + startSize_);

	// if header block type is either basic, extended, or external
	if (config_.HBlockInfo_.type_ != OAConfig::hbNone)
	{
		for (unsigned i = 0; i < config_.ObjectsPerPage_; ++i)
		{
			// if a block is in use
			if (GetHeaderFlag(address + (i * segmentSize_)))

				// return false, a block exists in the page that is in use
				return false;
		}
	}

	// if no header block was allocated
	else
	{
		for (unsigned i = 0; i < config_.ObjectsPerPage_; ++i)
		{
			// if address is not found on free list
			if (!CheckIfInFreeList(address + (i * segmentSize_)))

				// return false, a block exists in the page that is in use
				return false;
		}
	}

	// addresses in page are not in use
	return true;
}

/**************************************************************************/
/*!

\brief
	This is a helper function to check if an address is on the free list 

\param
	memory - beginning address of an allocate-able block

\return
	True if address exists on the free list, False if it doesn't exist on the
	list

*/
/**************************************************************************/
bool ObjectAllocator::CheckIfInFreeList(char * memory)
{
	// loop through FreeList_
	GenericObject *freeJumper = FreeList_;

	while (freeJumper != NULL)
	{
		// if an address match was found
		if (static_cast<void*>(freeJumper) == static_cast<void*>(memory))

			// block exists on the FreeList
			return true;

		// move pointer to next node
		freeJumper = freeJumper->Next;
	}

	// block does not exist on the FreeList
	return false;
}

/**************************************************************************/
/*!

\brief
	This is a static function to check if extra credit was implemented.
	
\return
	True if extra credit implementation was done, False if not

*/
/**************************************************************************/
bool ObjectAllocator::ImplementedExtraCredit()
{
	return true;
}

/**************************************************************************/
/*!

\brief
	This is a helper function to for debugging purposes to print the addresses
	of all pages that the memory manager has dynamically allocated from the
	operating system

*/
/**************************************************************************/
void ObjectAllocator::PrintPageList()
{
	GenericObject *pageJumper = PageList_;

	// loop through the linked list
	for (int i = 1; pageJumper != NULL; pageJumper = pageJumper->Next, ++i)
	{
		// print the address
		std::cout << "Page #" << i << ": " << 
			reinterpret_cast<void*>(pageJumper) << std::endl;

		// move to next page
		pageJumper = pageJumper->Next;
	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function for debugging purposes to print the address of
	all blocks that are available for allocation to the client.

*/
/**************************************************************************/
void ObjectAllocator::PrintFreeList()
{
	GenericObject *freeJumper = FreeList_;

	// loop through the linked list
	for (int i = 1; freeJumper != NULL; freeJumper = freeJumper->Next, ++i)
	{
		// print the address
		std::cout << "Free Block #" << i << ": " <<
			reinterpret_cast<void*>(freeJumper) << std::endl;

		// move to next item on free list
		freeJumper = freeJumper->Next;
	}
}

/**************************************************************************/
/*!

\brief
	This is a helper function for debugging purposes to print the addresses of
	every allocate-able block within a page.

\param
	memory - beginning address of an allocate-able block

*/
/**************************************************************************/
void ObjectAllocator::PrintPage(char * memory)
{
	// prints the page address
	std::cout << "Page Address: " << static_cast<void*>(memory) << std::endl;

	// point to first block
	char * address = memory + startSize_;

	// print all blocks within the page
	for (int i = 0; i < config_.ObjectsPerPage_; ++i)
	{
		std::cout << "Block #" << i << ": " <<
			static_cast<void*>(address + (i * segmentSize_)) << std::endl;
	}
}
