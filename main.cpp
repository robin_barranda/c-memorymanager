#include "ObjectAllocator.h"
#include <chrono>
#include <iostream>

class HeavyClass {
	double heavy[500];
};

int main()
{
	const int NUM_OBJS = 10000;

	OAConfig config(false, 500, 100, false );
	ObjectAllocator *obj;
	obj = new ObjectAllocator(sizeof(HeavyClass), config);

	// generate 500 objects with new
	HeavyClass **myHeavy = new HeavyClass*[NUM_OBJS];
	auto start = std::chrono::system_clock::now();
	//////////////////// 2 PASS NEW DELETE ///////////////
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		myHeavy[i] = new HeavyClass;
	}
	// delete all objects
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		delete myHeavy[i];
	}
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		myHeavy[i] = new HeavyClass;
	}
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		delete myHeavy[i];
	}
	//delete[] myHeavy;
	// end time
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = end - start;
	std::cout << "Allocations and deallocations with new: " << NUM_OBJS << "\nTime elapsed using new: " << elapsed_time.count() << std::endl << std::endl;

	// Using object allocator
	//myHeavy = new HeavyClass*[500];
	start = std::chrono::system_clock::now();
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		myHeavy[i] = reinterpret_cast<HeavyClass*>(obj->Allocate());
	}
	// delete all objects
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		obj->Free(myHeavy[i]);
	}
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		myHeavy[i] = reinterpret_cast<HeavyClass*>(obj->Allocate());
	}	
	// delete all objects
	for (int i = 0; i < NUM_OBJS; ++i)
	{
		obj->Free(myHeavy[i]);
	}
	//delete[] myHeavy;
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time2 = end - start;
	std::cout << "Allocs and deallocs with memory mgr: " << NUM_OBJS << "\nTime elapsed using object allocator: " << elapsed_time2.count() << std::endl << std::endl;
	std::cout << "Factor of memory mgr over C++ new: " << elapsed_time.count() / elapsed_time2.count() << std::endl;
	
	std::cin.get();

	return 0;
}